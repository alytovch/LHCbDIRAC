###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "LICENSE".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

# pylint: skip-file
# Used by Test_RunApplication.py (for integration test)

from ProdConf import ProdConf

ProdConf(
    NOfEvents=-1,
    DDDBTag="dddb-20150928",
    CondDBTag="sim-20160321-2-vc-mu100",
    AppVersion="v43r2p11",
    XMLSummaryFile="summaryBrunel_00033857_00000005_5.xml",
    Application="Brunel",
    OutputFilePrefix="00033857_00000005_5",
    XMLFileCatalog="pool_xml_catalog.xml",
    InputFiles=["LFN:00033857_00000004_4.digi"],
    OutputFileTypes=["dst"],
)
